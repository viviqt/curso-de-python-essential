# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 22:33:41 2021

@author: viviq
"""
"""
Su tarea es escribir y probar una función que toma dos argumentos (un año y un mes) y devuelve 
el número de días para el par mes / año dado (aunque solo febrero es sensible al valor del año, su función debería ser universal).

La parte inicial de la función está lista. Ahora, modifique a la función para que use la opción de return  None si sus argumentos 
no tienen sentido.

Por supuesto, puede (y debe) usar la función previamente escrita y probada (LAB Listas y return). 
Puede ser de mucha ayuda. Lo alentamos a que use una lista con los meses. 
Puede crearlo dentro de la función: este truco acortará significativamente el código.

Hemos preparado un código de prueba. 
"""
def isYearLeap(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return(True)
            else:
                return(False)
        else:
            return(True)
    else:
        return(False) 

def daysInMonth(year, month):
    treinta=[4,6,9,11]
    trentayuno=[1,3,5,7,8,10,12]
    febrero=2
    dias=0
    year = int(input('Introduce un año: '))
    month = int(input('Introduce un mes: '))
    if month in treinta:
        dias=30
        print(month)
    else:
        if month in trentayuno:
            dias=31
            print(month)
        else:
            if month is febrero:
                result = isYearLeap(year)
                if result is True:
                    dias=29
                    print(month)
                else:
                    dias=28
                    print(month)
    return(dias)


testYears = [1900, 2000, 2016, 1987]

testMonths = [2, 2, 1, 11]

testResults = [28, 29, 31, 30]

for i in range(len(testYears)):
    yr = testYears[i]
    mo = testMonths[i]
    print(yr, mo, "->", end="")
    result = daysInMonth(yr, mo)
    if result is testResults[i]:
        print("OK")
    else:
        print("Failed")
        
