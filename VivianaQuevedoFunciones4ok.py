# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 22:33:52 2021

@author: viviq
"""

"""
Su tarea es escribir y probar una función que toma tres argumentos (un año, un mes y un día del mes) y 
devuelve los días correspondiente del año, o devuelve None si alguno de los argumentos es inválido.

Use las funciones previamente escritas y probadas. Agregue algunos casos de prueba al código. 

Esta prueba es solo un comienzo.
"""

def isYearLeap(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return(True)
            else:
                return(False)
        else:
            return(True)
    else:
        return(False) 

def daysInMonth(year, month):
    treinta=[4,6,9,11]
    trentayuno=[1,3,5,7,8,10,12]
    febrero=2
    dias=0
    if month in treinta:
        dias=30
        print(month)
    else:
        if month in trentayuno:
            dias=31
            print(month)
        else:
            if month is febrero:
                result = isYearLeap(year)
                if result is True:
                    dias=29
                    print(month)
                else:
                    dias=28
                    print(month)
    return(dias)

def dayOfYear(year, month, day):
    if(month<=2):
        month = month + 12
        year = year - 1
    else:
        month = month - 2
    K = year % 100
    J = year / 1000
    h = ((700 + ((26 * month -2) / 10) + day + K + (K/4) + ((J/4) + 5*J))%7)
    dia =int(h)
    if dia == 1:
        print('lunes')
    if dia == 2:
        print('martes')
    if dia == 3:
        print('miércoles')
    if dia == 4:
        print('jueves')
    if dia == 5:
        print('viernes')
    if dia == 6:
        print('sábado')
    if dia == 7:
        print('domingo')


print(dayOfYear(2000, 12, 31))


